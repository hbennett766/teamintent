package edu.uchicago.hbennett766.teamintent;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private int CONTACT_REQUEST_CODE = 1;
    private int GOOGLE_REQUEST_CODE = 2;
    private int SEARCH_REQUEST_CODE = 3;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent);
            }
        }

        Button contactsButton = (Button) findViewById(R.id.contactsButton);
        contactsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectContact();
            }
        });

        Button googleButton = (Button) findViewById(R.id.googleButton);
        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                talkToGoogle();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CONTACT_REQUEST_CODE && resultCode == RESULT_OK) {
            uri = data.getData();
            getName();
            getEmail();
        }
        else if (requestCode == GOOGLE_REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String converted_text = (result.get(0));
            searchGoogle(converted_text);
        }
    }


    //
    // Select Contact Button
    //

    public void selectContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, Uri.parse("contents://contact"));
        intent.setType(ContactsContract.CommonDataKinds.Email.CONTENT_TYPE);
        startActivityForResult(intent, CONTACT_REQUEST_CODE);
    }

    public void getName() {
        String contactName = null;
        Cursor nameCursor = getContentResolver().query(uri, null, null, null, null);
        if (nameCursor.moveToFirst()) {
            contactName = nameCursor.getString(nameCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            TextView nameTextView = (TextView) findViewById(R.id.nameText);
            nameTextView.setText(contactName);
        }
        nameCursor.close();
    }

    public void getEmail() {
        String contactEmail = null;
        String email = ContactsContract.CommonDataKinds.Email.ADDRESS;
        String[] emailParams = {email};
        Cursor emailCursor = getContentResolver().query(uri, emailParams, null, null, null);
        if (emailCursor.moveToFirst()) {
            contactEmail = emailCursor.getString(emailCursor.getColumnIndex(email));
            TextView emailTextView = (TextView) findViewById(R.id.emailText);
            emailTextView.setText(contactEmail);
        }
        emailCursor.close();
    }

    //
    // Select Google Button
    //

    public void talkToGoogle() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        startActivityForResult(intent, GOOGLE_REQUEST_CODE);
    }

    public void searchGoogle(String searchText) {
        uri = Uri.parse("geo:0,0?q=" + Uri.encode(searchText));
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setPackage("com.google.android.apps.maps");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, SEARCH_REQUEST_CODE);
        }
    }

    public void handleSendText(Intent intent) {
        String data = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (data != null) {
            TextView placeTextView = (TextView) findViewById(R.id.dataText);
            placeTextView.setText(data);
        }
    }
}
